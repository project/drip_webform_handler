<?php

namespace Drupal\drip_webform_handler\Plugin\WebformHandler;

use DrewM\Drip\Dataset;
use DrewM\Drip\Drip;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use GuzzleHttp\Exception\RequestException;

/**
 * Drip Webform handler.
 *
 * @WebformHandler(
 *   id = "drip_webform_handler",
 *   label = @Translation("Drip.com Webform Handler"),
 *   category = @Translation("Transaction"),
 *   description = @Translation("Sends the submission data to Drip.com"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */

class DripWebformHandler extends WebformHandlerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
        'api_key' => '',
        'account_id' => '',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // API settings.
    $form['api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Drip.com API settings'),
      '#attributes' => ['id' => 'api-settings'],
    ];

    $form['api_settings']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drip.com API-key'),
      '#description' => $this->t('You can find your API Token on your Drip User Info page.'),
      '#default_value' => $this->configuration['api_key'],
    ];

    $form['api_settings']['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drip.com numeric Account ID.'),
      '#description' => $this->t('You can find your numeric account ID in your URL when visiting your Drip dashboard.'),
      '#default_value' => $this->configuration['account_id'],
    ];

    $form['api_settings']['tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tags.'),
      '#description' => $this->t('Provide tags as a pipe-separated value. Ex: tag1|tag2|tag3'),
      '#default_value' => $this->configuration['tags'],
    ];

    $form['api_settings']['eu_consent'] = [
      '#type' => 'select',
      '#title' => $this->t('GDPR-consent'),
      '#description' => $this->t('Force value GDPR-consent. Attention: When consent is granted, use this handler only with specific conditions.'),
      '#options' => [
        'granted' => $this->t('Granted'),
        'denied' => $this->t('Denied'),
        'unknown' => $this->t('Unknown'),
      ],
      '#default_value' => $this->configuration['eu_consent'],
    ];

    // Get field mapping options based on current webform elements.
    $options = $this->getMappingOptions();

    // Get the Drip-fields to which we can past values.
    $drip_fields = $this->getSubscriberFields();

    foreach ($drip_fields as $field_name => $field_description) {
      $form['api_settings']['fields'][$field_name] = [
        '#type' => 'select',
        '#title' => $field_description . ' [' . $field_name . ']',
        '#options' => ['' => $this->t('Please select')] + $options,
        '#default_value' => $this->configuration[$field_name],
      ];
    }

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values= $form_state->getValues();

    $this->configuration['api_key'] = $form_state->getValue('api_key');
    $this->configuration['account_id'] = $form_state->getValue('account_id');
    $this->configuration['tags'] = $values['api_settings']['tags'];
    $this->configuration['eu_consent'] = $values['api_settings']['eu_consent'];

    foreach ($values['api_settings']['fields'] as $key => $value) {
      $this->configuration[$key] = $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state,  WebformSubmissionInterface $webform_submission) {
    // Get the submission data.
    $values = $webform_submission->getData();

    // Create Drip Client.
    $client = new Drip(
      $this->configuration['api_key'],
      $this->configuration['account_id']
    );

    // Create Drip Dataset.
    $dataset = [
      'tags' => explode('|', $this->configuration['tags']),
      'eu_consent' => $this->configuration['eu_consent'],
    ];
    foreach ($this->getSubscriberFields() as $key => $name) {
      $dataset[$key] = $values[$this->configuration[$key]] ?? '';
    }

    $drip_dataset = new Dataset('subscribers', $dataset);

    try {
      $response = $client->post('subscribers', $drip_dataset);
    }
    catch (RequestException $request_exception) {

    }
  }

  /**
   * Returns an array of all the possible mapping options based on the current webform elements.
   */
  protected function getMappingOptions() {
    $options = [];

    $elements = $this->webform->getElementsDecodedAndFlattened();
    foreach($elements as $key => $element) {
      if(!in_array($element['#type'], ['textfield', 'email'])) {
        unset($elements[$key]);
      }
    }

    foreach($elements as $key => $element) {
      $options[$key] = $element['#title'];
    }

    return $options;
  }

  /**
   * Returns an array of all the subscriber properties.
   *
   * Properties taken from https://developer.drip.com/#subscribers
   */
  protected function getSubscriberFields() {
    return [
      'email' => $this->t('The subscriber\'s email address.'),
      'first_name' => $this->t('The subscriber\'s first name.'),
      'last_name' => $this->t('The subscriber\'s last name.'),
      'address1' => $this->t('The subscriber\'s mailing address.'),
      'address2' => $this->t('An additional field for the subscriber\'s mailing address.'),
      'city' => $this->t('The city, town, or village in which the subscriber resides.'),
      'state' => $this->t('The region in which the subscriber resides. Typically a province, a state, or a prefecture.'),
      'zip' => $this->t('The postal code in which the subscriber resides, also known as zip, postcode, Eircode, etc.'),
      'country' => $this->t('The country in which the subscriber resides.'),
      'phone' => $this->t('The subscriber\'s primary phone number.'),
      'time_zone' => $this->t('The subscriber\'s time zone (in Olson format).'),
      'ip_address' => $this->t('The subscriber\'s IP address if available.'),
    ];
  }

}
