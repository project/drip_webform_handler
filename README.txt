Drip Webform Handler
===========
Drip Webform Handler provides a new Webform Handler plugin to send submission data to Drip via their API.


Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8


Configuration
------------

Add a new handler of type "Drip Webform Handler" to the webform.
Add your Drip API-key and Account ID and fill in the rest of the field mappings.
Tou can also add tags to your submissions by using the Tag-field.